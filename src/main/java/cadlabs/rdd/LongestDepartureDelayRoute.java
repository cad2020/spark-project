package cadlabs.rdd;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.distributed.BlockMatrix;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;
import scala.Tuple2;

import java.util.List;
import java.util.function.Function;


public class LongestDepartureDelayRoute extends AbstractFlightAnalyser<Tuple2<List<Tuple2<String, Double>>, Double>> {
    public LongestDepartureDelayRoute(JavaRDD<Flight> flights) {
        super(flights);
    }

    @Override
    public Tuple2<List<Tuple2<String, Double>>, Double> run() {
        // TODO
        // Store flights in adjacency matrix
        // Compute All-Pairs Shortest Path
        // Grab the route with the biggest delay

        JavaRDD<MatrixEntry> entries =
                this.flights.map(flight -> new MatrixEntry(flight.org_id, flight.dest_id, flight.depdelaymins));
        CoordinateMatrix coordMat = new CoordinateMatrix(entries.rdd());

        BlockMatrix adjacencyMat = coordMat.toBlockMatrix().cache();

        return null;
    }
}
