package cadlabs.rdd;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.util.List;

public class FlightRouteGraph {

    private static final String DEFAULT_FILE = "data/flights.csv";

    private static SparkSession spark;

    private static void startSpark() {
        // start Spark session (SparkContext API may also be used)
        // master("local") indicates local execution
        spark = SparkSession.builder().
                appName("FlightRouteGraph").
                master("local").
                getOrCreate();


        // only error messages are logged from this point onward
        // comment (or change configuration) if you want the entire log
        spark.sparkContext().setLogLevel("ERROR");
    }

    private static JavaRDD<Flight> processInputFile(String file) {
        JavaRDD<String> textFile = spark.read().textFile(file).javaRDD();
        return textFile.map(Flight::parseFlight).cache();
    }

    public static void main(String[] args) {
        String file;

        if (args.length < 1) {
            System.err.println("Usage: FlightRouteGraph <file>");
            System.err.println("No file given, assuming " + DEFAULT_FILE);
            file = DEFAULT_FILE;
        } else {
            file = args[0];
        }

        startSpark();
        JavaRDD<Flight> flights = processInputFile(file);

        Tuple2<List<Tuple2<String, Double>>, Double> results = new LongestDepartureDelayRoute(flights).run();
        List<Tuple2<String, Double>> route = results._1;

        System.out.println("Route with more delay at departure time:");
        for(int i = 0; i < route.size(); i++) {
            String airport = route.get(i)._1;
            int delayMins = route.get(i)._2.intValue();

            String output;
            if(i == route.size() - 1) {
                output = String.format("%s (%dm)", airport, delayMins);
            } else {
                output = String.format("%s (%dm) -> ", airport, delayMins);
            }

            System.out.println(output);
        }

        System.out.printf("Total delay: %dm\n", results._2.intValue());
    }
}
